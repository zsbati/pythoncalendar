# Enter your code here. Read input from STDIN. Print output to STDOUT
import calendar
date = input()
date1 = date.split(" ")
month = int(date1[0])
day = int(date1[1])
year = int(date1[2])
weekday = calendar.weekday(year, month, day)
if weekday == 0:
    print("MONDAY")
if weekday == 1:
    print("TUESDAY")
if weekday == 2:
    print("WEDNESDAY")
if weekday == 3:
    print("THURSDAY")
if weekday == 4:
    print("FRIDAY")
if weekday == 5:
    print("SATURDAY")
if weekday == 6:
    print("SUNDAY")        

